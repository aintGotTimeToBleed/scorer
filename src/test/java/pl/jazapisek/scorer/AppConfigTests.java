package pl.jazapisek.scorer;

import com.arangodb.ArangoDB;
import com.arangodb.entity.ArangoDBVersion;
import com.arangodb.springframework.core.ArangoOperations;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author mzapisek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppConfigTests {
    
    @Autowired
    ArangoDB.Builder arangoDbBuilder;
    
    @Autowired
    private ArangoOperations operations;
    
    @Test
    public void arangoDbBuilderShouldNotBeNull(){
        assertNotNull(arangoDbBuilder);
    }
    
    @Test
    public void isValidArangoDbVersion() {
        assertEquals(operations.getVersion().getVersion(), "3.3.10");
    }
    
}
