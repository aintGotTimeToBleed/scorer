package pl.jazapisek.scorer.repository;

import pl.jazapisek.scorer.repository.RuleDefinitionRepository;
import com.arangodb.springframework.core.ArangoOperations;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import pl.jazapisek.scorer.entity.RuleDefinition;

/**
 *
 * @author mzapisek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RuleDefinitionRepositoryTests {

    @Autowired
    private ArangoOperations operations;

    @Autowired
    private RuleDefinitionRepository ruleDefinitionRepository;

    @After
    @Before
    public void dropCollection(){
        operations.collection(RuleDefinition.class).drop();
    }
    
    @Test
    public void operationsShouldNotBeNull(){
        assertNotNull(operations);
    }
    
    @Test
    public void ruleDefinitionRepositoryShouldNotBeNull() {
        assertNotNull(ruleDefinitionRepository);
    }
    
    @Test
    public void ruleDefinitionShouldBeInserded_shouldReturnOne(){
        RuleDefinition ruleDefinition = new RuleDefinition();
        ruleDefinition.setRuleName("noncontact");
        ruleDefinition.setRuleNamePl("szkoda bezkontaktowa");
        ruleDefinition.setRuleType("negative");
        ruleDefinition.setCategoryLevel(5);
        ruleDefinition.setUsed(true);
        ruleDefinitionRepository.save(ruleDefinition);
        assertEquals(1L, ruleDefinitionRepository.count());
    }

    /**
     * Zakładamy, że może być tylko jedna reguła o tej samej nazwie.
     */
    @Test(expected = DataIntegrityViolationException.class)
    public void ruleDefinitionWithTheSameName_shouldThrowDataIntegrityException(){
        RuleDefinition r1 = new RuleDefinition();
        r1.setRuleName("noncontact");
        r1.setRuleNamePl("szkoda bezkontaktowa");
        r1.setRuleType("negative");
        r1.setCategoryLevel(5);
        r1.setUsed(true);
        ruleDefinitionRepository.save(r1);
        
        RuleDefinition r2 = new RuleDefinition();
        r2.setRuleName("noncontact");
        r2.setRuleNamePl("szkoda bezkontaktowa");
        r2.setRuleType("negative");
        r2.setCategoryLevel(5);
        r2.setUsed(true);
        ruleDefinitionRepository.save(r1);
    }
    
}
