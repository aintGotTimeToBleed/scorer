package pl.jazapisek.scorer.data;

import pl.jazapisek.scorer.service.XmlParser;
import pl.jazapisek.scorer.service.DomXmlParser;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import pl.jazapisek.scorer.service.Helper;

/**
 *
 * @author mzapisek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DomXmlParserTests {

    @Autowired
    @Qualifier("xmlParsingService")
    private XmlParser xmlParser;

    @Test
    public void xmlParserShouldNotBeNull() {
        assertNotNull(xmlParser);
    }

    @Test
    public void xmlParserShouldBeInstanceOfXmlDomParser() {
        assertTrue(xmlParser instanceof DomXmlParser);
    }

    @Test
    public void shouldGetValidDocument() throws Exception {
        String xml = Helper.getFileAsString("src/main/resources/xml/anonymised_claim_1.xml");
        Document document = xmlParser.parse(xml);

        assertNotNull(document);
        assertTrue(document instanceof Document);
    }

    @Test
    public void shouldGetValidNode() throws Exception {
        String xml = Helper.getFileAsString("src/main/resources/xml/anonymised_claim_1.xml");
        Document document = xmlParser.parse(xml);
        Node eventDescriptionNode = document
                .getElementsByTagName("eventDescription")
                .item(0);
        assertNotNull(eventDescriptionNode);

        Element eventDescriptionNodeElement = (Element) eventDescriptionNode;
        assertEquals(
                eventDescriptionNodeElement.getTextContent(),
                "OC - pojazd sprawcy wymusił pierwszeństwo na pojeździe "
                + "poszkodowanym, sprawca zawracał z niedozwolonego "
                + "pasa. Status pojazdu: jezdny");
    }

}
