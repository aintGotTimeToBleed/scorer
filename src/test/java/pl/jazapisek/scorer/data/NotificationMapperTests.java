package pl.jazapisek.scorer.data;

import pl.jazapisek.scorer.service.Notification;
import pl.jazapisek.scorer.service.NotificationMapper;
import pl.jazapisek.scorer.service.DomNotificationMapper;
import pl.jazapisek.scorer.service.XmlParser;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import pl.jazapisek.scorer.service.Helper;

/**
 *
 * @author mzapisek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NotificationMapperTests {
    
    @Autowired
    @Qualifier("xmlParsingService")
    XmlParser xmlParser;
    
    private String xml;
    
    @Before
    public void getClaimXml() throws Exception{
        xml = Helper.getFileAsString("src/main/resources/xml/anonymised_claim_1.xml");
    }
    
    @Test
    public void xmlParserBeanShouldNotBeNull(){
        assertNotNull(xmlParser);
    }
    
    @Test
    public void shouldFindDamageDescriptionNode() throws Exception {
        String xml = "<xml><iClaim><iClaimData><damageDescription>"
                + "zderzak"
                + "</damageDescription></iClaimData></iClaim></xml>";
        Document document = xmlParser.parse(xml);
        NotificationMapper notificationMapper = 
                new DomNotificationMapper(new Notification(), document);
        notificationMapper.mapDamageDescription();
        Notification notification = notificationMapper.getNotification();
        
        assertEquals("zderzak", notification.getDamageDescription());
    }

    @Test
    public void shouldNotFindDamageDescriptionNode() throws Exception {
        String xml = "<xml><iClaim><iClaimData><damageDescriptionn>"
                + "zderzak"
                + "</damageDescriptionn></iClaimData></iClaim></xml>";
        Document document = xmlParser.parse(xml);
        NotificationMapper notificationMapper = 
                new DomNotificationMapper(new Notification(), document);
        notificationMapper.mapDamageDescription();
        Notification notification = notificationMapper.getNotification();
        
        assertNull(notification.getDamageDescription());
    }
    
    
}
