package pl.jazapisek.scorer.controller;

import pl.jazapisek.scorer.controller.MainController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.hamcrest.Matchers;
import pl.jazapisek.scorer.service.Helper;

/**
 *
 * @author mzapisek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MainControllerTests {

    @Autowired
    MainController mainController;

    private MockHttpServletRequestBuilder postBuilder;
    private MockMvc mockMvc;

    @Before
    public void before() {
        this.mockMvc = standaloneSetup(mainController).build();
    }

    @Test
    public void calculateScoring_shouldReturnEmptyXmlMessage() throws Exception {
        this.postBuilder = MockMvcRequestBuilders
                .post("/calculateScoring")
                .param("notificationXml", "");

        mockMvc.perform(postBuilder)
                .andExpect(jsonPath("$.message", Matchers.is("XML param is empty")));
    }

    @Test
    public void calculateScoring_shouldReturnX() throws Exception {
        String fileLocation = "src/main/resources/xml/anonymised_claim_1.xml";

        this.postBuilder = MockMvcRequestBuilders
                .post("/calculateScoring")
                .param("notificationXml", Helper.getFileAsString(fileLocation));

        this.mockMvc.perform(postBuilder)
                .andExpect(jsonPath("$.message", Matchers.is("ok")));
    }

}
