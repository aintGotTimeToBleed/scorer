package pl.jazapisek.scorer.entity;

import pl.jazapisek.scorer.entity.RuleDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 *
 * @author mzapisek
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RuleDefinitionTests {

    /**
     * Sprawdzamy czy ktoś przez przypadek nie zmienił struktury encji.
     * Jeśli zmieni się struktura to trzeba zmodyfikować test i wszystkie
     * wykorzystania.
     */
    @Test
    public void testRuleDefinitionStructure() {
        RuleDefinition ruleDefinition = new RuleDefinition();
        ruleDefinition.setRuleName("noncontact");
        ruleDefinition.setRuleNamePl("szkoda bezkontaktowa");
        ruleDefinition.setRuleType("negative");
        ruleDefinition.setCategoryLevel(2);
        ruleDefinition.setUsed(true);
        
        String expected = "RuleDefinition(id=null, "
                + "ruleName=noncontact, "
                + "ruleNamePl=szkoda bezkontaktowa, "
                + "ruleType=negative, "
                + "categoryLevel=2, "
                + "used=true)";
        
        assertEquals(ruleDefinition.toString(), expected);
    }

}
