package pl.jazapisek.scorer;

import com.arangodb.ArangoDB;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author mzapisek
 */
@Configuration
@Profile("development")
@EnableArangoRepositories(basePackages = {"pl.jazapisek.scorer"})
public class AppConfig extends AbstractArangoConfiguration {
    
    @Bean
    @Override
    public ArangoDB.Builder arango() {
        ArangoDB.Builder arango = new ArangoDB.Builder()
                .host("localhost", 8529)
                .user("root")
                .password("admin");
        return arango;
    }

    @Bean
    @Override
    public String database() {
        return "LISProperty";
    }

}
