package pl.jazapisek.scorer.controller;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.jazapisek.scorer.service.Response;
import pl.jazapisek.scorer.service.XmlParser;

/**
 *
 * @author mzapisek
 */
@RestController
public class MainController {

    private final XmlParser xmlParser;

    @Autowired
    public MainController(
            @Qualifier(value = "xmlParsingService") XmlParser xmlParser) {
        this.xmlParser = xmlParser;
    }

    @RequestMapping(
            path = "/calculateScoring", 
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response calculateScoring(
            @RequestParam(name = "notificationXml", required = true) String xml)
            throws IOException, Exception {

        Response response = new Response();
        response.setMessage("ok");
        
        if (xml.isEmpty()){
            response.setMessage("XML param is empty");
            return response;
        }
        
        return response;
    }

}
