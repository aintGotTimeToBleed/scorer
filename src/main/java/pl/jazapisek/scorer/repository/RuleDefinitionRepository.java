package pl.jazapisek.scorer.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import pl.jazapisek.scorer.entity.RuleDefinition;

/**
 *
 * @author mzapisek
 */
public interface RuleDefinitionRepository extends ArangoRepository<RuleDefinition>{
    
}
