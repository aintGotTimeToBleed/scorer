package pl.jazapisek.scorer.scoring;

import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import pl.jazapisek.scorer.service.Notification;

/**
 *
 * @author mzapisek
 */
public class BasicRules {
    
    public static BiPredicate<Notification, Params> isAc = (t, u) -> {
        return t.equals(u);
    };
    
}
