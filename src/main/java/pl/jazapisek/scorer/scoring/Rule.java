package pl.jazapisek.scorer.scoring;

import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Data
public class Rule {
    
    private String ruleName;
    private boolean result;
    
}
