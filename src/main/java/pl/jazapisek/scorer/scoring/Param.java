package pl.jazapisek.scorer.scoring;

import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Data
public class Param<T> {

    private String name;
    private T value;

}
