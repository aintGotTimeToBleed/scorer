package pl.jazapisek.scorer.entity;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.HashIndex;
import java.io.Serializable;
import javax.persistence.Id;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 *
 * @author mzapisek
 */
@Document("rulesDefinitions")
@HashIndex(fields = {"ruleName", "ruleNamePl"}, unique = true)
@Data
public class RuleDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    public RuleDefinition() {
        super();
    }

    @Id
    @Setter(AccessLevel.NONE)
    private String id;

    private String ruleName;
    private String ruleNamePl;
    private String ruleType;
    private int categoryLevel;
    private boolean used;

}
