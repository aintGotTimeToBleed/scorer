package pl.jazapisek.scorer.service;

import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Data
public class Notification {

    private String eventDescription;
    private LocalDateTime insuranceStartDate;
    private LocalDateTime insuranceEndDate;
    private LocalDateTime policySignDate;
    private LocalDateTime eventDate;
    private LocalDateTime notificationDate;
    private String damageDescription;
    
}
