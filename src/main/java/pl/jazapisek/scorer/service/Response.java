package pl.jazapisek.scorer.service;

import lombok.Data;

/**
 *
 * @author mzapisek
 */
@Data
public class Response {

    private String message;
    private int score;
    
}
