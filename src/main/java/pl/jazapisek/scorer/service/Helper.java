package pl.jazapisek.scorer.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 *
 * @author mzapisek
 */
public class Helper {

    /**
     * Funckja pomocnicza tworząca stringa z pliku ze wskazanej lokalizacji.
     *
     * @param filePathString String - ścieżka do pliku
     * @return String - xml jako string
     */
    public static String getFileAsString(String filePathString) throws Exception {
        Path filePath = Paths.get(filePathString);

        StringBuilder sb = new StringBuilder();
        try (Stream<String> stream = Files.lines(filePath, StandardCharsets.UTF_8)) {
            stream.forEach((t) -> {
                sb.append(t);
            });
        } catch (IOException ex) {
            throw ex;
        } finally {
            return sb.toString();
        }
    }
    
}
