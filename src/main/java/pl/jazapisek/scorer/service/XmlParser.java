package pl.jazapisek.scorer.service;

import org.w3c.dom.Document;

/**
 *
 * @author mzapisek
 */
public interface XmlParser {

    public Document parse(String xml) throws Exception;

}
