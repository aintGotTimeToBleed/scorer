package pl.jazapisek.scorer.service;

import java.time.LocalDateTime;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author mzapisek
 */
public class DomNotificationMapper implements NotificationMapper {

    private Notification notification;
    private final Document document;

    public DomNotificationMapper(Notification notification, Document document) {
        this.notification = new Notification();
        this.document = document;
    }

    @Override
    public Notification getNotification() {
        return notification;
    }

    @Override
    public Document getDocument() {
        return document;
    }

    /**
     *
     * @param nodeTagName
     * @return
     */
    public String findSingleNodeValueInDocument(String nodeTagName) {
        Node node = document.getElementsByTagName(nodeTagName).item(0);
        Element nodeValue = (Element) node;

        return nodeValue.getTextContent();
    }

    /**
     *
     *
     * @param nodeTagName
     * @param parentNodeName
     * @return
     */
    public String findSingleNodeValueInDocument(
            String nodeTagName, String parentNodeName) {
        NodeList nodeList = document.getElementsByTagName(nodeTagName);
        for (int i = 0; nodeList.getLength() > i; i++) {
            Node node = nodeList.item(i);
            if (node.getParentNode().getNodeName().equals(parentNodeName)) {
                Element nodeValue = (Element) node;
                return nodeValue.getTextContent();
            }
        }

        return null;
    }

    public void mapEventDescription() {
        notification.setEventDescription(
                findSingleNodeValueInDocument("eventDescription"));
    }

    public void mapEventDate() {
        notification.setEventDate(
                LocalDateTime.parse(findSingleNodeValueInDocument("eventDate")));
    }

    public void mapNotificationDate() {
        notification.setNotificationDate(LocalDateTime.MIN);
    }

    @Override
    public void mapDamageDescription() {
        notification.setDamageDescription(
                findSingleNodeValueInDocument("damageDescription", "iClaimData"));
    }

}
