package pl.jazapisek.scorer.service;

import org.w3c.dom.Document;

/**
 *
 * @author mzapisek
 */
public interface NotificationMapper {
       
    Notification getNotification();
    
    Document getDocument();
    
    void mapDamageDescription();
    
}
