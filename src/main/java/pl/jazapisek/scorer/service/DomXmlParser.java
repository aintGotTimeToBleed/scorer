package pl.jazapisek.scorer.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author mzapisek
 */
@Service
@Qualifier("xmlParsingService")
public class DomXmlParser implements XmlParser {

    @Override
    public Document parse(String xml) throws Exception {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            BufferedInputStream bis
                    = new BufferedInputStream(
                            new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
            return builder.parse(bis);

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            ex.printStackTrace();
            throw ex;
        }

    }

}
